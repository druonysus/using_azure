Notes on using [Azure](https://portal.azure.com)
====

## Terminology & Jargon

When learning new tech, I like to start with trying to understand some of the terms and jargon that has formed to communicate the concepts I am learning. I find that the words used frequenly influence my mental-model of the tools and problem domains I will be working with. I will jot words and definitions down in this section. I will add and refine definitions as I come to understand them. This section is not intended to be exhaustive.

 + Location(s) - 

 + Virtual Machine Scale Set

 + Kubernetes Services
 
 + SQL Databases

 + KeyVault

 + Data Warehouses

 + Kusto

 + Cloud Storage

 + Azure Monitor
   - Logs (perviously known as Log Analytics)
   - Log Analytics workspaces

 + Azure Resource Manager
   - Resource
   - Resource Group (Sometimes, just Group)
   - Resource Provider
   - Resource Manager template

 + SignalR Service

 + Service Fabric

 + Security Center

 + DevTest Labs

 ## Azure Hosted Third-patry Products

 + Kubernetes `az aks ...` 
 + OpenShift `az openshift ...`
 + MySQL `az mysql ...`
 + MariaDB `az mariadb ...`
 + Redis `az redis ...`

## Azure CLI (`az`)

### [Installing](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest)

#### On [openSUSE Leap](https://software.opensuse.org/distributions/leap) 15.1

##### From openSUSE's standard repos
Either as root, or using `sudo` install all available `azure-cli` packages. This will insure you have all modules installed.

```
zypper install 'azure-cli*'
```

**NOTE:** _As of this writing, when running the `az` command provided by the openSUSE RPMs, there is always an error produced of `Error loading command module 'taskhelp'`. While this might look like an issue, it doesn't seem to effect functionality._

##### From [Microsoft's repos](https://packages.microsoft.com/yumrepos/azure-cli) (recommended)

Try running the command below, but if this doesn't work, you should follow [Microsoft's instructions.](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-zypper?view=azure-cli-latest)

```
zypper addrepo \
    --name "Azure CLI" \
    --check --refresh https://packages.microsoft.com/yumrepos/azure-cli \
    azure-cli && \
        zypper --gpg-auto-import-keys refresh azure-cli && \
        zypper --non-interactive install --from azure-cli --force azure-cli
```

**NOTE:** _Using the Microsoft provided RPMs seem to not have the issue of showing the `Error loading command module 'taskhelp` error._

### Login

```
 az login
```

### Configure

For a CLI wizard-like experiance, run `az configure`. Otherwise just edit `~/.azure/config` directly. I highly recommend setting `output = table`.

### Resources & Resource Groups

#### Creating a Resource Group

```
 az group create \
    --location westus \
    --name ${USER}NewRG \
    --tags 'env[=devtest]'
```

### Disks

#### Creating

```
az disk create \
    --name newStorageDisk \
    --resource-group drewsNewGroup \
    --location westus \
    --os-type Linux \
    --size-gb 50
```

### Virtual Machine

#### Get a list of available OS images

**NOTE:** _If you ask for a list of all available OS images (as with`az vm image list --all`) it will take some time for your shell prompt to return. It is recommended that you narrow the scope of your search with the `--offer` or `--publisher` options so as not to wait so long for a result._

##### All from SUSE
```
az vm image list \
    --publisher SUSE \
    --all
```

#### Create

Once you decide on the VM image and Resource Group you are going to use, it is very simple to create a new VM.

```
 az vm create \                   
    --resource-group ${USER}NewGroup \
    --name testLeapVM001 \
    --image openSUSE-Leap \
    --admin-username $USER \
    --ssh-key-value "$(ssh-add -L)"
```

#### Show IP Addresses Assoicated with a VM

```
az vm list-ip-addresses -n myLeapVM
```

#### Accessing your VMs
You can `ssh` to VMs in Azure, just as you would any machine. Just use the `PublicIPAddresses` value of the output of the `... vm list-ip-addresses ...` command shown in the above section.

```
ssh <your user>@<public addresse shown in output of above section>
```

**Example**
```
ssh drew_adams@13.81.159.21
```

### Kubernetes

#### Getting available versions

```
az aks get-versions --location westus
```

#### Creating a cluster

**NOTE:** _When creating a cluster,_`- Running ..` _will be displayed at your terminal for some time and it will not return until the cluster is created or an error is produced. In other words, when successfully creating a new cluster you prompt maybe take 10 or more minutes to return. Be patient._

```
az aks create \
    --resource-group myK8sGroup \
    --name myK8sCluster \
    --node-count 1 \
    --kubernetes-version 1.12.8 \
    --admin-username $USER \
    --ssh-key-value "$(ssh-add -L)"
```
Creating a cluster 

#### Show Dashboard in the broswer

**NOTE:** _This requires your machine to have `kubectl` installed and in your user's `PATH`. If you do not already have `kubectl` you can very easily install it to `/usr/local/bin/` by running `az aks install-cli` as root or with `sudo`._

```
az aks browse \
    --name myK8sCluster \
    --resource-group myK8sGroup
```
---
Copyright © 2019, 2023 Drew Adams <<druonysus@opensuse.org>>.

This material has been released under and is subject to the terms of the
Common Documentation License, v.1.0, the terms of which are hereby
incorporated by reference. Please obtain a copy of the License at
https://spdx.org/licenses/CDL-1.0.html and read it before using this
material. Your use of this material signifies your agreement to the terms of
the License.
